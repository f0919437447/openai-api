package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type openai_req struct {
	Model             string   `json:"model"`
	Prompt            string   `json:"prompt"`
	Temperature       float32  `json:"temperature"`
	Max_tokens        int      `json:"max_tokens"`
	Top_p             int      `json:"top_p"`
	Frequency_penalty int      `json:"frequency_penalty"`
	Presence_penalty  float32  `json:"presence_penalty"`
	Stop              []string `json:"stop"`
}

type openai_resp struct {
	Id      string              `json:"id"`
	Object  string              `json:"object"`
	Created int                 `json:"create"`
	Model   string              `json:"model"`
	Choices []map[string]string `json:"choices"`
	Usage   map[string]int      `json:"usage"`
}

func main() {

	b, err := os.ReadFile("./openAI_API_Key")
	if err != nil {
		return
	}
	API_KEY := string(b)
	client := &http.Client{}

	data := openai_req{
		Model:             "text-davinci-003",
		Prompt:            "",
		Temperature:       0.9,
		Max_tokens:        150,
		Top_p:             1,
		Frequency_penalty: 0,
		Presence_penalty:  0.6,
		Stop:              []string{" Human:", " AI:"},
	}

	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter question: ")
		prompt, _ := reader.ReadString('\n')

		data.Prompt = prompt
		body, err := json.Marshal(data)
		_ = err
		req, err := http.NewRequest("POST", "https://api.openai.com/v1/completions", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Authorization", "Bearer "+API_KEY)

		resp, err := client.Do(req)
		body, err = ioutil.ReadAll(resp.Body)

		respData := &openai_resp{}
		json.Unmarshal(body, respData)
		fmt.Println(respData.Choices[0]["text"])
	}
}

// curl https://api.openai.com/v1/completions \
//   -H "Content-Type: application/json" \
//   -H "Authorization: Bearer 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" \
//   -d '{
//   "model": "text-davinci-003",
//   "prompt": "",
//   "temperature": 0.9,
//   "max_tokens": 150,
//   "top_p": 1,
//   "frequency_penalty": 0,
//   "presence_penalty": 0.6,
//   "stop": [" Human:", " AI:"]
// }'
